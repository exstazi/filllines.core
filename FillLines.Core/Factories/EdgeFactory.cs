﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Factories
{
    public class EdgeFactory
    {
        public static Element.Edge CreateEdge(Enum.EdgeEnum type, int id, Element.Node firstNode, Element.Node secondNode)
        {
            switch (type)
            {
                case Enum.EdgeEnum.Simple:
                    return new Element.Edge(firstNode, secondNode);

                case Enum.EdgeEnum.NonReturnValve:
                    return new Element.NonReturnValve(firstNode, secondNode);

                case Enum.EdgeEnum.Valve:
                    return new Element.Valve(firstNode, secondNode);

                case Enum.EdgeEnum.Pump:
                    return new Element.Pump(id, firstNode, secondNode);

                case Enum.EdgeEnum.Stub:
                    return new Element.Stub(firstNode, secondNode);
            }
            return null;
        }
    }
}
