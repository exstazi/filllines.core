﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Factories
{
    public class NodeFactory
    {
        public static Element.Node CreateNode(Enum.NodeEnum type)
        {
            switch (type)
            {
                case Enum.NodeEnum.Pipe:
                    return new Element.Node();

                case Enum.NodeEnum.Source:
                    return new Element.Source();

                case Enum.NodeEnum.Consumer:
                    return new Element.Consumer();
            }
            return null;
        }
    }
}
