﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FillLines.Core.Model;

namespace FillLines.Core.Element
{
    public class Node
    {
        public Dictionary<Source, FillPoint> FillList;
        public NodeInfo Info { get; protected set; }

        protected List<Edge> connectingEdges;        

        public Node()
        {
            connectingEdges = new List<Edge>();
            FillList = new Dictionary<Source, FillPoint>();
            Info = new NodeInfo()
            {
                FuelType = 0,
                Pressure = false,
                Suction = false
            };
        }

        public void AddEdge(Edge edge)
        {
            connectingEdges.Add(edge);
        }

        public virtual void FillingNode(Edge sender, Source source, int depth)
        {
            if (!FillList.ContainsKey(source))
            {
                FillList.Add(source, new FillPoint(depth + 1));
                UpdateFillList();
                source.FuelTypeChanged += SourceFuelTypeChanged;
            }
            else
            {
                if (FillList[source].depthLine > depth + 1)
                {
                    FillList[source].depthLine = depth + 1;
                }
                else
                {
                    return;
                }
            }
            foreach (var e in connectingEdges)
            {
                if (e != sender)
                {
                    e.FillingEdge(this, source, FillList[source].depthLine);
                }
            }
        }

        public virtual void CleanNode(Edge sender, Source source, int depth)
        {
            if (FillList.ContainsKey(source))
            {
                int oldDepth = FillList[source].depthLine;
                if (oldDepth > depth)
                {
                    FillList.Remove(source);
                    UpdateFillList();
                    source.FuelTypeChanged -= SourceFuelTypeChanged;

                    foreach (var e in connectingEdges)
                    {
                        if (e != sender)
                        {
                            e.CleanEdge(this, source, depth);
                        }
                    }
                }
                else
                {
                    sender.FillingEdge(this, source, FillList[source].depthLine);
                    foreach (var pump in FillList[source].fillingPumps)
                    {
                        sender.StartPumpingEdge(this, source, pump);
                    }
                }
            }
        }

        public virtual void StartPumpingNode(Edge sender, Source source, PumpFilling pump)
        {
            if (FillList.ContainsKey(source))
            {
                if (FillList[source].fillingPumps.Contains(pump))
                {
                    return;
                }

                FillList[source].fillingPumps.Add(pump);
                UpdateFillList();

                foreach (var e in connectingEdges)
                {
                    if (e != sender)
                    {
                        e.StartPumpingEdge(this, source, pump);
                    }
                }
            }
        }

        public virtual void StopPumpingNode(Edge sender, Source source, PumpFilling pump)
        {
            if (FillList.ContainsKey(source))
            {
                if (FillList[source].fillingPumps.Remove(pump))
                {
                    UpdateFillList();

                    foreach (var e in connectingEdges)
                    {
                        if (e != sender)
                        {
                            e.StopPumpingEdge(this, source, pump);
                        }
                    }
                }
            }
        }

        private void UpdateFillList()
        {
            Info.FuelType = 0;
            Info.Pressure = false;
            Info.Suction = false;

            foreach (var item in FillList)
            {
                if (Info.FuelType == 0)
                {
                    Info.FuelType = item.Key.FuelType;
                } else if (Info.FuelType > 0 && Info.FuelType != item.Key.FuelType)
                {
                    Info.FuelType = -1;
                }

                foreach (var pumping in item.Value.fillingPumps)
                {
                    if (pumping.pressure)
                    {
                        Info.Pressure = true;
                    } else
                    {
                        Info.Suction = true;
                    }
                }
            }
        }

        private void SourceFuelTypeChanged(int fuelType)
        {
            UpdateFillList();
        }
    }
}
