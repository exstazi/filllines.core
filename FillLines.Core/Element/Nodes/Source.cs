﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FillLines.Core.Model;

namespace FillLines.Core.Element
{
    public class Source : Node
    {
        public int FuelType { get; protected set; }

        public delegate void FuelTypeHandler(int fuelType);
        public event FuelTypeHandler FuelTypeChanged;
     
        public Source()
            : base()
        {
            FuelType = 0;

            FillList.Add(this, new FillPoint(0));
        }

        public void FillingLines()
        {
            foreach (var e in connectingEdges)
            {
                e.FillingEdge(this, this, 0);
            }
        }

        public void ChangeFuelType(int fuelType)
        {
            FuelType = fuelType;
            FuelTypeChanged?.Invoke(fuelType);
        }

        public override void FillingNode(Edge sender, Source source, int depth)
        {
            if (!FillList.ContainsKey(source))
            {
                FillList.Add(source, new FillPoint(depth + 1));
            }
            else
            {
                if (FillList[source].depthLine > depth + 1)
                {
                    FillList[source].depthLine = depth + 1;
                }
                else
                {
                    return;
                }
            }
        }

        public override void CleanNode(Edge sender, Source source, int depth)
        {
            if (FillList.ContainsKey(source))
            {
                int oldDepth = FillList[source].depthLine;
                if (oldDepth > depth)
                {
                    FillList.Remove(source);
                }
                else
                {
                    sender.FillingEdge(this, source, FillList[source].depthLine);
                    foreach (var pump in FillList[source].fillingPumps)
                    {
                        sender.StartPumpingEdge(this, source, pump);
                    }
                }
            }
        }

        public override void StartPumpingNode(Edge sender, Source source, PumpFilling pump)
        {
            if (FillList.ContainsKey(source))
            {
                if (FillList[source].fillingPumps.Contains(pump))
                {
                    return;
                }
                FillList[source].fillingPumps.Add(pump);
            }
        }

        public override void StopPumpingNode(Edge sender, Source source, PumpFilling pump)
        {
            if (FillList.ContainsKey(source))
            {
                FillList[source].fillingPumps.Remove(pump);
            }
        }
    }
}
