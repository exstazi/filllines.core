﻿using FillLines.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Element
{
    class NonReturnValve: Edge
    {
        public NonReturnValve(Node source, Node destination)
            : base(source, destination)
        { }

        public override void FillingEdge(Node sender, Source source, int depth)
        {
            if (sender == firstNode)
            {
                secondNode?.FillingNode(this, source, depth);
            }
        }
        public override void StartPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if ((sender == firstNode) && (secondNode != null) && (pump.pressure))
            {
                secondNode.StartPumpingNode(this, source, pump);
            }
            else if ((sender == secondNode) && (firstNode != null) && (!pump.pressure))
            {
                firstNode.StartPumpingNode(this, source, pump);
            }
        }
    }
}
