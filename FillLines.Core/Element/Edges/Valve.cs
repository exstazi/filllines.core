﻿using FillLines.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Element
{
    public class Valve: Edge
    {
        public bool IsOpen { get; protected set; }

        public Valve(Node source, Node dest)
            :base(source, dest)
        {
            IsOpen = false;
        }

        public override void FillingEdge(Node sender, Source source, int depth)
        {
            if (IsOpen)
            {
                base.FillingEdge(sender, source, depth);
            }
        }
        public override void CleanEdge(Node sender, Source source, int depth)
        {
            if (IsOpen)
            {
                base.CleanEdge(sender, source, depth);
            }
        }
        public override void StartPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if (IsOpen)
            {
                base.StartPumpingEdge(sender, source, pump);
            }
        }
        public override void StopPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if (IsOpen)
            {
                base.StopPumpingEdge(sender, source, pump);
            }
        }

        public override void ChangeStatus(int status)
        {
            if (firstNode != null && secondNode != null)
            {
                if (IsOpen == true && status == 0)
                {
                    IsOpen = false;
                    //Filling
                    var copyFillList = new Dictionary<Source, FillPoint>(firstNode.FillList);
                    foreach (var source in copyFillList.Keys)
                    {
                        if (secondNode.FillList.ContainsKey(source))
                        {
                            if (secondNode.FillList[source].depthLine > firstNode.FillList[source].depthLine)
                            {
                                secondNode.CleanNode(this, source, firstNode.FillList[source].depthLine);
                            }
                            else if (firstNode.FillList[source].depthLine > secondNode.FillList[source].depthLine)
                            {
                                firstNode.CleanNode(this, source, secondNode.FillList[source].depthLine);
                            }
                        }
                    }

                    //Pumping
                    foreach (var source in firstNode.FillList.Keys)
                    {
                        var copyPumpList = new List<PumpFilling>(firstNode.FillList[source].fillingPumps);
                        foreach (PumpFilling pump in copyPumpList)
                        {
                            firstNode.StopPumpingNode(this, source, pump);
                        }
                    }
                    foreach (var source in secondNode.FillList.Keys)
                    {
                        var copyPumpList = new List<PumpFilling>(secondNode.FillList[source].fillingPumps);
                        foreach (var pump in copyPumpList)
                        {
                            secondNode.StopPumpingNode(this, source, pump);
                        }
                    }
                }
                else if (IsOpen == false && status != 0)
                {
                    IsOpen = true;
                    //Filling
                    var copyFillList = new Dictionary<Source, FillPoint>(firstNode.FillList);
                    foreach (var source in copyFillList.Keys)
                    {
                        if (!secondNode.FillList.ContainsKey(source)
                            || secondNode.FillList[source].depthLine > firstNode.FillList[source].depthLine)
                        {
                            secondNode.FillingNode(this, source, firstNode.FillList[source].depthLine);
                        }
                        else if (firstNode.FillList[source].depthLine > secondNode.FillList[source].depthLine)
                        {
                            firstNode.FillingNode(this, source, secondNode.FillList[source].depthLine);
                        }

                    }
                    copyFillList = new Dictionary<Source, FillPoint>(secondNode.FillList);
                    foreach (var source in copyFillList.Keys)
                    {
                        if (!firstNode.FillList.ContainsKey(source))
                        {
                            firstNode.FillingNode(this, source, secondNode.FillList[source].depthLine);
                        }
                    }

                    //Pumping
                    foreach (var source in firstNode.FillList.Keys)
                    {
                        var copyPumpList = new List<PumpFilling>(firstNode.FillList[source].fillingPumps);
                        foreach (PumpFilling pump in copyPumpList)
                        {
                            secondNode.StartPumpingNode(this, source, pump);
                        }
                    }
                    foreach (var source in secondNode.FillList.Keys)
                    {
                        var copyPumpList = new List<PumpFilling>(secondNode.FillList[source].fillingPumps);
                        foreach (PumpFilling pump in copyPumpList)
                        {
                            firstNode.StartPumpingNode(this, source, pump);
                        }
                    }
                }
            }
        }

        public override int GetCurrentStatus()
        {
            return IsOpen ? 1 : 0;
        }
    }
}
