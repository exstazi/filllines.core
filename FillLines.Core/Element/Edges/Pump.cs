﻿using FillLines.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Element
{
    class Pump: Edge
    {
        public bool IsRun { get; protected set; }

        private PumpFilling inPump;
        private PumpFilling outPump;

        public Pump(int id, Node source, Node destination)
            : base(source, destination)
        {
            IsRun = false;
            inPump = new PumpFilling(id, false);
            outPump = new PumpFilling(id, true);
        }

        public override void FillingEdge(Node sender, Source source, int depth)
        {
            if (IsRun)
            {
                if (sender == firstNode)
                {
                    secondNode?.FillingNode(this, source, depth);
                    secondNode?.StartPumpingNode(this, source, outPump);
                    firstNode.StartPumpingNode(this, source, inPump);
                }
            }
            else
            {
                base.FillingEdge(sender, source, depth);
            }
        }

        public override void StartPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if ((pump != inPump) && (pump != outPump))
            {
                if (!IsRun)
                {
                    base.StartPumpingEdge(sender, source, pump);
                }
                else
                {
                    if ((sender == firstNode) && (secondNode != null) && (pump.pressure))
                    {
                        secondNode.StartPumpingNode(this, source, pump);
                    }
                    else if ((sender == secondNode) && (firstNode != null) && (!pump.pressure))
                    {
                        firstNode.StartPumpingNode(this, source, pump);
                    }
                }
            }
            else if (!IsRun)
            {
                firstNode.StopPumpingNode(this, source, inPump);
                secondNode?.StopPumpingNode(this, source, outPump);
            }
        }
        public override void StopPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if ((pump != inPump) && (pump != outPump))
            {
                base.StopPumpingEdge(sender, source, pump);
            }
            else if (IsRun)
            {
                firstNode.StartPumpingNode(this, source, inPump);
                secondNode?.StartPumpingNode(this, source, outPump);
            }
        }
        public override void ChangeStatus(int status)
        {
            if (firstNode != null)
            {
                if (status != 0)
                {
                    if (!IsRun)
                    {
                        IsRun = true;
                        // Filling like NonReturnValve
                        var copyFillList = new Dictionary<Source, FillPoint>(firstNode.FillList);
                        foreach (var source in copyFillList.Keys)
                        {
                            if (secondNode.FillList.ContainsKey(source))
                            {
                                if (firstNode.FillList[source].depthLine > secondNode.FillList[source].depthLine)
                                {
                                    firstNode.CleanNode(this, source, secondNode.FillList[source].depthLine);
                                }
                            }
                        }
                        //Pumping
                        foreach (var source in firstNode.FillList.Keys)
                        {
                            var copyPumpList = new List<PumpFilling>(firstNode.FillList[source].fillingPumps);
                            foreach (PumpFilling pump in copyPumpList)
                            {
                                firstNode.StopPumpingNode(this, source, pump);
                            }
                        }
                        foreach (var source in secondNode.FillList.Keys)
                        {
                            var copyPumpList = new List<PumpFilling>(secondNode.FillList[source].fillingPumps);
                            foreach (PumpFilling pump in copyPumpList)
                            {
                                secondNode.StopPumpingNode(this, source, pump);
                            }
                        }
                        //Pumping this pump
                        foreach (var source in firstNode.FillList.Keys)
                        {
                            secondNode?.StartPumpingNode(this, source, outPump);
                            firstNode.StartPumpingNode(this, source, inPump);
                        }
                    }
                }
                else 
                {
                    if (IsRun)
                    {
                        IsRun = false;
                        //Filling like simple edge
                        var copyFillList = new Dictionary<Source, FillPoint>(secondNode.FillList);
                        foreach (var source in copyFillList.Keys)
                        {
                            if ((!firstNode.FillList.ContainsKey(source)) || (firstNode.FillList[source].depthLine > secondNode.FillList[source].depthLine))
                            {
                                firstNode.FillingNode(this, source, secondNode.FillList[source].depthLine);
                            }
                        }

                        //Pumping others pump
                        foreach (var source in secondNode.FillList.Keys)
                        {
                            var copyPumpList = new List<PumpFilling>(secondNode.FillList[source].fillingPumps);
                            foreach (PumpFilling pump in copyPumpList)
                            {
                                if ((pump != inPump) && (pump != outPump))
                                {
                                    firstNode.StartPumpingNode(this, source, pump);
                                }
                            }
                        }
                        foreach (var source in firstNode.FillList.Keys)
                        {
                            var copyPumpList = new List<PumpFilling>(firstNode.FillList[source].fillingPumps);
                            foreach (PumpFilling pump in copyPumpList)
                            {
                                if ((pump != inPump) && (pump != outPump))
                                {
                                    secondNode.StartPumpingNode(this, source, pump);
                                }
                            }
                        }
                        //Pumping this pump
                        foreach (var source in firstNode.FillList.Keys)
                        {
                            secondNode?.StopPumpingNode(this, source, outPump);
                            firstNode.StopPumpingNode(this, source, inPump);
                        }
                    }
                }
            }
        }

        public override int GetCurrentStatus()
        {
            return IsRun ? 1 : 0;
        }
    }
}
