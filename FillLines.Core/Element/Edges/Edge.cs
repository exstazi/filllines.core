﻿using FillLines.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Element
{
    public class Edge
    {
        protected Node firstNode;
        protected Node secondNode;

        public Edge(Node firstNode, Node secondNode)
        {
            this.firstNode = firstNode;
            this.secondNode = secondNode;

            this.firstNode.AddEdge(this);
            this.secondNode.AddEdge(this);
        }

        public virtual void FillingEdge(Node sender, Source source, int depth)
        {
            if (sender == firstNode)
                secondNode?.FillingNode(this, source, depth);
            else
                firstNode?.FillingNode(this, source, depth);
        }

        public virtual void CleanEdge(Node sender, Source source, int depth)
        {
            if (sender == firstNode)
                secondNode?.CleanNode(this, source, depth);
            else
                firstNode?.CleanNode(this, source, depth);
        }

        public virtual void StartPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if (sender == firstNode)
                secondNode?.StartPumpingNode(this, source, pump);
            else
                firstNode.StartPumpingNode(this, source, pump);
        }

        public virtual void StopPumpingEdge(Node sender, Source source, PumpFilling pump)
        {
            if (sender == firstNode)
                secondNode?.StopPumpingNode(this, source, pump);
            else
                firstNode.StopPumpingNode(this, source, pump);
        }

        public virtual void ChangeStatus(int status)
        { }

        public virtual int GetCurrentStatus()
        {
            return 0;
        }
    }
}
