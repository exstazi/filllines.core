﻿using FillLines.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Element
{
    class Stub: Edge
    {
        public Stub(Node firstNode, Node secondNode)
            : base(firstNode, secondNode)
        { }

        public override void FillingEdge(Node sender, Source source, int depth) { }
        public override void CleanEdge(Node sender, Source source, int depth) { }
        public override void StartPumpingEdge(Node sender, Source source, PumpFilling pump) { }
        public override void StopPumpingEdge(Node sender, Source source, PumpFilling pump) { }
    }
}
