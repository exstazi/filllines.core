﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Model
{
    public class PumpFilling
    {
        public int pump;
        public bool pressure;
        public PumpFilling(int p, bool press)
        {
            pump = p;
            pressure = press;
        }
    }
}
