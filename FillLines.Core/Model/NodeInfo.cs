﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Model
{
    public class NodeInfo
    {
        public int FuelType { get; set; }
        public bool Pressure { get; set; }
        public bool Suction { get; set; }
    }
}
