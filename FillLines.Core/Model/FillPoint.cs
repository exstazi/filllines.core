﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLines.Core.Model
{
    public class FillPoint
    {
        public int depthLine;
        public List<PumpFilling> fillingPumps;
        public FillPoint()
        {
            depthLine = 0;
            fillingPumps = new List<PumpFilling>();
        }
        public FillPoint(int depth)
        {
            depthLine = depth;
            fillingPumps = new List<PumpFilling>();
        }
        public FillPoint(int depth, List<PumpFilling> pumps)
        {
            depthLine = depth;
            fillingPumps = new List<PumpFilling>(pumps);
        }
    }
}
